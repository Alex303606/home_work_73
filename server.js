const express = require('express');
const Caesar = require('caesar-salad').Caesar;

const app = express();
const port = 8000;

app.listen(port, () => {
	console.log('We are live on ' + port);
});

app.get('/', (req, res) => {
	res.send('Hello, world!\n');
});
app.post('/', (req, res) => {
	res.send('SUCCESS!!!\n');
});

app.get('/:name', (req, res) => {
	res.send(`Hello ${req.params.name}`);
});

app.get('/encode/:name', (req, res) => {
	res.send(Caesar.Cipher('password').crypt(req.params.name));
});

app.get('/decode/:name', (req, res) => {
	res.send(Caesar.Decipher('password').crypt(req.params.name));
});